package com.wex.microdsl.maven.plugin;

import com.wex.microdsl.Microdsl;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * @goal generate
 * @phase install
 */
public class WexTemplateRefinery extends AbstractMojo {

    /**
     * @parameter expression="${project}"
     * @required
     */
    private MavenProject project;

    /**
     * @parameter expression="${dslPath}"
     */
    private String dslPath = "deploy.microdsl";

    /**
     * @parameter expression="${awsTemplate}"
     */
    private String awsTemplate = "./aws";

    /**
     * @parameter expression="${skip}"
     */
    private boolean skip = false;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (!skip) Microdsl.generate(dslPath, awsTemplate, project);
    }
}
