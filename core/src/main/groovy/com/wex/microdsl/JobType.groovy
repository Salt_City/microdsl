package com.wex.microdsl

/**
 * Enumeration that defines what type of job we are trying to run when we run this program. This is mostly to
 *  allow a user to run the `generate` function without having to have a current AWS security token.
 */
enum JobType {
    GENERATE,
    DEPLOY
}