package com.wex.microdsl.generator

import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.CaseUtils

/**
 * This is the class responsible for generating the DSL spec for all of the AWS YAML
 *  files defined in the resource directory of this project
 */
class DslSpecGenerator {

    /**
     * Generate will go through the resource files and find all directories under `templates/service`. These directories
     *  represent a domain of AWS (ec2, ecs, rds, etc..), and all resources that belong to that domain are defined in
     *  these directories. Generate will go through the directories, make the corresponding directory in the project, and
     *  generate all resources and a base resource class that maintains all the resources for the component
     */
    def generate() {
        def baseGroovyName = "./core/src/main/groovy/com/wex/microdsl/generate/aws/"
        new File(getClass().getResource("/templates/service").toURI()).eachDir { d ->
            new File(baseGroovyName + d.getName()).mkdir()
            d.listFiles().each {
                def groovy = new File("${baseGroovyName}${d.getName()}/${it.getName().replace(".yml", "")}Spec.groovy")
                if (!groovy.exists()) {
                    def bindings = it.text.findAll(~'\\$\\{.*}').collect{it.replace("\${", "").replace("}", "")}.unique()
                    groovy << getGroovy(d.getName(), it.getName().replace(".yml", "Spec"), bindings)
                }
            }
            def base = new File(baseGroovyName + d.getName() + "/${CaseUtils.toCamelCase(d.getName(), true)}Spec.groovy")
            base.text = ''
            base << buildParentSpec(d.getName(), "${CaseUtils.toCamelCase(d.getName(), true)}Spec", d.listFiles().collect{it.getName().replace(".yml", "")})
        }
    }

    /**
     * The parent Spec is the class that holds all definitions for a specific resource. These classes also contain several
     *  helper functions.
     * @param packageName - The package name for the Base Spec
     * @param className - The name of the Class being generated
     * @param specs - The list of specs the Base spec class will define
     * @return - Returns a String representation of a Class
     */
    def buildParentSpec(String packageName, String className, List<String> specs) {
        def builder = new StringBuilder()

        builder.append("package com.wex.microdsl.generate.aws.${packageName}").append("\n\n")
            .append("import com.wex.microdsl.generate.aws.BaseSpec").append("\n")
            .append("import com.wex.microdsl.generate.aws.BaseResourceSpec").append("\n\n")
            .append("class ${className} implements BaseSpec {").append("\n\n")

        specs.each {
            def lowerCase = StringUtils.lowerCase(it)
            builder.append("\t").append("List<${it}Spec> ${lowerCase}s = []").append("\n")
        }
        builder.append("\n\n")

        specs.each{
            def lowerCase = StringUtils.lowerCase(it)
            def firstCharLower = StringUtils.lowerCase(it.charAt(0).toString())
            def methodName = "${firstCharLower}${it[1..it.length()-1]}"
            builder.append("\t").append("def ${methodName}(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ${it}Spec) Closure ${lowerCase}Closure) {").append("\n")
                .append("\t\t").append("def specification = new ${it}Spec()").append("\n")
                .append("\t\t").append("def c = ${lowerCase}Closure.rehydrate(specification, this, this)").append("\n")
                .append("\t\t").append("c()").append("\n")
                .append("\t\t").append("${lowerCase}s << specification").append("\n")
                .append("\t").append("}").append("\n\n")
        }

        builder.append("\t").append("@Override").append("\n")
        builder.append("\t").append("def getResources() {").append("\n")

        specs.eachWithIndex{ s, i ->
            builder.append("\t\t")
            if (i == 0) {
                if (specs.size() > 1) builder.append("[${StringUtils.lowerCase(s)}s,")
                else builder.append("[${StringUtils.lowerCase(s)}s]")
            }
            else if (i == specs.size()-1) builder.append("${StringUtils.lowerCase(s)}s]")
            else builder.append("${StringUtils.lowerCase(s)}s,")
            builder.append("\n")
        }

        builder.append("\t").append("}").append("\n\n")

        builder.append("\t").append("@Override").append("\n")
        builder.append("\t").append("def <T extends BaseSpec> void consume(T sub) {").append("\n")
            .append("\t\t").append("assert sub instanceof ${className}").append("\n")

        specs.each {
            builder.append("\t\t")

            builder.append("${StringUtils.lowerCase(it)}s += sub.${StringUtils.lowerCase(it)}s").append("\n")
        }

        builder.append("\t").append("}").append("\n\n")

        builder.append("\t").append("@Override").append("\n")
        builder.append("\t").append("List<BaseResourceSpec> getResourceByName(String name) {").append("\n")
        builder.append("\t\t").append("switch (name) {").append("\n")
        specs.each{
            builder.append("\t\t\t").append("case \"${StringUtils.lowerCase(it)}\" :").append("\n")
            builder.append("\t\t\t\t").append("${StringUtils.lowerCase(it)}s").append("\n")
            builder.append("\t\t\t\t").append("break").append("\n")
        }

        builder.append("\t\t\t").append("default :").append("\n")
        builder.append("\t\t\t\t").append("null").append("\n")
        builder.append("\t\t").append("}").append("\n")
        builder.append("\t").append("}").append("\n\n")


        builder.append("\t").append("@Override").append("\n")
        builder.append("\t").append("def generate() {").append("\n")
            .append("\t\t").append("def builder = new StringBuilder()").append("\n\n")

        specs.each {
            def lowerCase = StringUtils.lowerCase(it)
            builder.append("\t\t").append("${lowerCase}s.each{builder.append(it.generate()).append(\"\\n\")}").append("\n")
        }
        builder.append("\t\t").append("builder.toString()").append("\n")
                .append("\t").append("}").append("\n")
                .append("}").toString()
    }

    /**
     * Get Groovy generates the groovy DSL spec for a sepcific resource, converting all EL tags to method definitions
     *  that take a String value, which the class will add to the binding that will be passed to the template generator
     * @param packageName - The package name for the class
     * @param className - The name of the Class being generated
     * @param bindings - The list of bindings to generate
     * @retrun - Returns a String representation of a Class
     */
    def getGroovy(String packageName, String className, List<String> bindings) {
        def builder = new StringBuilder()

        builder.append("package com.wex.microdsl.generate.aws.${packageName}").append("\n\n")
            .append("import com.wex.microdsl.generate.aws.BaseResourceSpec").append("\n\n")
            .append("class ${className} extends BaseResourceSpec {").append("\n\n")

        bindings.each{
            builder.append("\t").append("def ${it}(String ${it}) {").append("\n")
                .append("\t\t").append("binding[\"${it}\"] = ${it}").append("\n")
                .append("\t").append("}").append("\n\n")
        }
        builder.append("}").toString()
    }
}
