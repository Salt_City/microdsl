package com.wex.microdsl.generator

/**
 * Bootstrap helper class that will generate DSL specs from AWS YAML specs.
 * This is useful because now the user can focus of crafting the AWS YAML spec just like they would if they were
 * writing a template. Any values they want to delegate to the service to fill they can set with an EL tag `${<key>}`, and
 * it will be picked up by the parser, which will generate a DSL for it
 */
class Bootstrap {
    public static void main(String[] args) {
        new DslSpecGenerator().generate()
    }


}
