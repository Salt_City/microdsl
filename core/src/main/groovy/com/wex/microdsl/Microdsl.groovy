package com.wex.microdsl

import com.wex.microdsl.model.ChildDeploySpec
import org.apache.maven.project.MavenProject

/**
 * Entry point API
 */
class Microdsl {
    static String environment = "dit"
    static MavenProject project;
    static JobType jobType

    static def deploy(String dslPath, MavenProject project, String environment) {
        jobType = JobType.DEPLOY
        this.environment = environment
        this.project = project
        def deploySpec = getDeploySpec(dslPath)
        deploySpec.executeService()

    }

    static def generate(String dslPath, String awsPath, MavenProject project) {
        jobType = JobType.GENERATE
        this.project = project
        def deploySpec = getDeploySpec(dslPath)

        def aws = new File(awsPath + "/service.yml")
        aws.text = ''
        aws << deploySpec.generateSpec.generateService()

        "rm -r parent".execute().waitForProcessOutput()
    }

    private static def getDeploySpec(String dslPath) {
        def f = new File(dslPath).text
        def c = new GroovyShell().evaluate("microdsl = {${f}}")
        microdsl(c)
    }

    static def microdsl(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ChildDeploySpec) Closure deploy) {
        ChildDeploySpec deploySpec = new ChildDeploySpec()
        def d = deploy.rehydrate(deploySpec, this, this)
        d()
        deploySpec
    }
}
