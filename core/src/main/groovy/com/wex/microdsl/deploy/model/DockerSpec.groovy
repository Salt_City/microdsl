package com.wex.microdsl.deploy.model

import groovy.transform.Immutable

/**
 * unused at the moment
 */
@Immutable class DockerSpec {
    String registry
    String repository
}
