package com.wex.microdsl.deploy.model

/**
 * Specification for AWS regions. The user can use environment mappings, or simply set a global region if they
 *  are vertically operating in a single region for all their environments
 */
class RegionSpec {
    String region
    HashMap<String, String> regions = [:]

    def region(String region) {
        this.region = region
    }

    def environmentMapping(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EnvironmentMappingSpec) Closure cl) {
        def e = cl.rehydrate(new EnvironmentMappingSpec(), this, this)
        e()
        this.regions = e.getMapping()

    }

    def getRegion(String env) {
        region ?: regions[env]
    }
}
