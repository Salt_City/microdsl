package com.wex.microdsl.deploy.model

import com.wex.microdsl.deploy.aws.AwsDeploy
import com.wex.microdsl.model.DeploySpec

/**
 * Template Specification for a child template.
 *
 * All templates that are not pulled in specifically as a
 * parent are considered a child.
 *
 * This was implemented in this way because Groovy DSL is parsed in a
 * recursive descent, which would cause StackOverflow errors when both the
 * child and the parent were of the same spec.
 */
class ChildTemplateSpec extends TemplateSpec {
    ParentTemplateSpec parent = new ParentTemplateSpec()

    /**
     * Sets the parent of this spec
     * @param parent - The parent template specification
     */
    def setParent(ParentTemplateSpec parent) {
        this.parent = parent
        this.sync(parent)

    }

    /**
     * Deploys the service template
     * @return - Returns a boolean indicating whether the deployment was a success or failure
     */
    def deployService() {
        deploy(DeploySpec.stackName, templatePath)
    }

    /**
     * generic deploy function
     * @param stackName - The name of the CloudFormation stack
     * @param templatePath - The path to the template
     * @param p - The list of parameters for the template
     * @return - Returns a boolean indicating whether the deployment was a success or failure
     */
    def deploy(String stackName, String templatePath) {
        def p = getParameters() + parent.getParameters()
        def gchat =  gChatSpec ?: parent.gChatSpec
        def complete = AwsDeploy.deployCreateOrUpdate(stackName, templatePath, p, tags.toList() + parent.tags.toList(),
                (parent.region.getRegion(environment) ?: region.getRegion(environment)),profile.getProfile(environment))
        if (gchat) {
            if (complete) gchat.sendSuccess()
            else gchat.sendFailure()
        }
        complete
    }
}
