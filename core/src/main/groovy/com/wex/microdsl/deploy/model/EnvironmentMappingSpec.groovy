package com.wex.microdsl.deploy.model

/**
 * Provides a mapping to the user when they might have several AWS regions to support under different environments
 * example:
 * environmentMapping {
 *             mapping "dit:us-east-1"
 *             mapping "sit:us-east-1"
 *             mapping "stage:us-west-2"
 *             mapping "prod:us-west-2"
 *         }
 */
class EnvironmentMappingSpec {
    HashMap<String, String> mapping = [:]

    def mapping(String m) {
        def (env, region) = m.tokenize(":")
        mapping[env] = region
    }

    def getMapping() {
        mapping
    }
}
