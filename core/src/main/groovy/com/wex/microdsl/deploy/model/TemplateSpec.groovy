package com.wex.microdsl.deploy.model

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.cloudformation.AmazonCloudFormationClientBuilder
import com.amazonaws.services.cloudformation.model.DescribeStackResourcesRequest
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest
import com.amazonaws.services.cloudformation.model.ListStackResourcesRequest
import com.amazonaws.services.cloudformation.model.Output
import com.amazonaws.services.cloudformation.model.Parameter
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClientBuilder
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeRulesRequest
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClient
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest
import com.wex.microdsl.AwsUtil
import com.wex.microdsl.Microdsl
import com.wex.microdsl.model.DeploySpec

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import java.nio.charset.StandardCharsets
import java.security.Key

/**
 * Base specification for deploying an AWS template
 * @todo separate model from impl
 */
class TemplateSpec {
    final static String DEFAULT_TEMPLATE = "./server/aws/service.yml"
    String environment = Microdsl.environment
    AmazonCloudFormationClient cloudClient
    AmazonElasticLoadBalancingClient elbClient
    AWSSimpleSystemsManagementClient ssmClient

    List<Parameter> parameters = []
    String templatePath = DEFAULT_TEMPLATE
    String team = DeploySpec.team
    String stackName = DeploySpec.stackName
    String serviceName = DeploySpec.serviceName
    Map<String, Properties> springProperties

    TagsSpec tags = new TagsSpec()
    RegionSpec region = new RegionSpec()
    DockerSpec docker = new DockerSpec()
    GChatSpec gChatSpec
    ProfileSpec profile = new ProfileSpec()

    def tags(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = TagsSpec) Closure tagClosure) {
        def t = tagClosure.rehydrate(tags, this, this)
        t()
    }

    def gchat(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = GChatSpec) Closure gChatClosure) {
        gChatSpec = new GChatSpec(this)
        def g = gChatClosure.rehydrate(gChatSpec, this, this)
        g()
    }

    def region(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RegionSpec) Closure regionClosure) {
        def r = regionClosure.rehydrate(region, this, this)
        r()
    }

    def docker(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = DockerSpec) Closure dockerClosure) {
        def d = dockerClosure.rehydrate(docker, this, this)
        d()
    }

    def profile(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = DockerSpec) Closure profilesClosure) {
        def p = profilesClosure.rehydrate(profile, this, this)
        p()
    }

    def templatePath(String templatePath) {
        this.templatePath = templatePath
    }

    def parameter(String p) {
        placeParam(p, parameters)
    }

    private def placeParam(String p, List<Parameter> l) {
        def (key, value) = tokenize(p)
        l << new Parameter().withParameterKey(key).withParameterValue(value)
    }

    private def tokenize(String p) {
        def l = p.tokenize("=")
        def key = l[0]
        def value = ""
        for (i in 1..l.size()-1) { value += l[i]}
        [key, value]
    }

    /**
     * Helper function to allow a microdsl file to retrieve a value from a static config file set for that env
     * @param name - The name of the config
     * @param key - The key to search on
     * @return - Returns the value associated with the key provided
     */
    def getConfig(String name, String key) {
        DeploySpec.configMappings[name][key]
    }

    /**
     * Helper function to allow a microdsl file to find an output value in any core stack. This is useful because
     *  it still allows a user to defined outputs in their core stack files, but the service stacks can now retrieve and
     *  use the values without the hard dependency lock that AWS forces when using the ImportValue function in the
     *  AWS template
     * @param core - The core stack (although technically this can retrieve an output from any stack)
     * @param output - The output key to search on
     * @return - Returns the output value associated with the key provided
     */
    def findInCore(String core, String output) {
        def stackOutput = AwsUtil.safeCall {
            getCloudClient().describeStacks(new DescribeStacksRequest().withStackName(core))
                    .getStacks().collect{it.getOutputs()}.flatten().find{(it as Output).getOutputKey() == output}
        }
        (stackOutput as Output).getOutputValue()
    }

    /**
     * Helper function to allow a microdsl file to find an ARN for a resource that is a part of another stack.
     * @param stack - The name of the stack to search
     * @param resource - The logical name of the resource (logical name is just the name you gave it in the CF template)
     * @return - Returns the ARN of a resource
     */
    def findStackResource(String stack, String resource) {
        AwsUtil.safeCall {
            getCloudClient().describeStackResources(new DescribeStackResourcesRequest().withStackName(stack))
                    .getStackResources().find{it.logicalResourceId == resource}.physicalResourceId
        }
    }

    /**
     * Helper function to allow a microdsl file to get all resources for a stack
     * @param stackName - The name of the stack
     * @return - Returns a List of StackResources Objects
     */
    def getStackResources(String stackName) {
        AwsUtil.safeCall {
            getCloudClient().describeStackResources(new DescribeStackResourcesRequest().withStackName(stackName))
                    .getStackResources()
        }
    }

    /**
     * Why not get a stack too while you're at it? Helper function to allow a microdsl file to get a Stack object from
     *  AWS based on the stack name
     * @param stackName - The name of the stack
     * @return - Returns a Stack Object
     */
    def getStack(String stackName) {
        AwsUtil.safeCall {
            getCloudClient().describeStacks(new DescribeStacksRequest().withStackName(stackName))
                    .getStacks().find{it.stackName == stackName}
        }
    }

    /**
     * Helper function to allow a microdsl file to get a priority number for a specfic ALB listener. It will first attempt
     *  to find the rule in the listener, and give you that priority number back, otherwise it will generate a new
     *  number
     * @param core - The core stack containing the ALB listener
     * @param listenerResource - The logical name of the ALB listener
     * @param condition - The rule condition
     */
    def getPriority(core, listenerResource, condition) {
        def p = getCurrentPriority(core, listenerResource, condition)  ?: generatePriority(core, listenerResource)
    }

    def getCurrentPriority( core, listenerResource, condition) {
       AwsUtil.safeCall {
           getElbClient().describeRules(new DescribeRulesRequest().withListenerArn(getCloudClient()
                   .listStackResources(new ListStackResourcesRequest().withStackName(core))
                   .getStackResourceSummaries().find{it.logicalResourceId == listenerResource}.physicalResourceId))
                   .getRules().find{it.conditions.find{ it.values.find{it.contains(condition)}}}?.priority
       }
    }

    def generatePriority(core, listenerResource) {
        def p = Math.abs(new Random().nextInt(50000))
        def check = AwsUtil.safeCall {
            getElbClient().describeRules(new DescribeRulesRequest().withListenerArn(getCloudClient()
                    .listStackResources(new ListStackResourcesRequest().withStackName(core))
                    .getStackResourceSummaries().find{it.logicalResourceId == listenerResource}.physicalResourceId))
                    .getRules().find{it.priority == Integer.toString(p)}
        }
        if (check) generatePriority(core, listenerResource)
        else p
    }

    /**
     * Helper function to allows a microdsl file to retrieve a value from a Spring property file
     * @param propFile - The name of the property file `ie: application-dit`
     * @param key - The key to search for in the file
     * @return - Returns the value
     */
    def getSpringProperty(String propFile, String key) {
        getProperties()[propFile].get(key)
    }

    /**
     * Helper function that allows a microdsl file to retrieve a value from a Spring property file following the common
     *  convention 'application-<env>'
     * If the property is not found in the environment specific property file, it will search the base application property
     *  file.
     * @param key - The key to search for
     * @return - Returns the value
     */
    def getSpringProperty(String key) {
        getProperties()["application-${environment}"].get(key) ?: getProperties()["application"].get(key)
    }

    /**
     * Helper function to allow a microdsl file to retreive a value from the maven properties set in the project POM file
     * @param key - The key to search on
     * @return - Returns the value associated with the key
     */
    def findMavenProperty(String key) {
        Microdsl.project.getProperties().get(key)
    }

    /**
     * Helper function to allow a microdsl file to return the version set in the Maven project
     * @return - Returns the version from the Maven project
     */
    def getMavenVersion() {
        Microdsl.project.getVersion()
    }

    def getParameters() {
        processParams(parameters)
    }

    def getServiceParameters() {
        processParams(serviceParameters)
    }

    def getDatabaseParameters() {
        processParams(databaseParameters)
    }

    def processParams(List<Parameter> p) {
        def runtime = processRuntimeParameters(p)
        processCipherParameters(runtime)
    }

    def getProperties() {
        if (springProperties) springProperties
        else {
            springProperties = [:]
            if (new File("${DeploySpec.springPropertyLocation}/application.properties").exists()) {
                new File("${DeploySpec.springPropertyLocation}/").listFiles().each {
                    if (it.name.contains(".properties")) {
                        def props = new Properties()
                        it.withInputStream { props.load(it)}
                        springProperties[it.name.replace(".properties", "")] = props
                    }
                }

            }
            springProperties
        }
    }

    def getElbClient() {
        if (elbClient) elbClient
        else {
            def elbBuilder = AmazonElasticLoadBalancingClientBuilder.standard().withRegion(region.getRegion(environment))
            if (profile.getProfile(environment)) elbBuilder.setCredentials(new ProfileCredentialsProvider(profile.getProfile(environment)))
            elbClient = elbBuilder.build()
            elbClient
        }
    }

    def getCloudClient() {
        if (cloudClient) cloudClient
        else {
            def cloudBuilder = AmazonCloudFormationClientBuilder.standard().withRegion(region.getRegion(environment))
            if (profile.getProfile(environment)) cloudBuilder.setCredentials(new ProfileCredentialsProvider(profile.getProfile(environment)))
            cloudClient = cloudBuilder.build()
            cloudClient
        }
    }

    def getSsmClient() {
        if (ssmClient) ssmClient
        else {
            def ssmBuilder = AWSSimpleSystemsManagementClientBuilder.standard().withRegion(region.getRegion(environment))
            if (profile.getProfile(environment)) ssmBuilder.setCredentials(new ProfileCredentialsProvider(profile.getProfile(environment)))
            ssmClient = ssmBuilder.build()
            ssmClient
        }
    }

    /**
     * Processes any params denoted with `{runtime: ...}`. This allows a user to control when a function is called
     *  for a required template parameter. If the user sets a param value with the runtime keyword, then the closure for
     *  the value will be executed just as the template is about to deploy, instead of when the deploy file was parsed.
     *  Useful for params that are time sensitive (ie: I need a security group for a database template deploying right
     *  before this, or I want the priority value to be set right before a deploy to lower my chance of priority collision
     *  during concurrent service deployments)
     * @param p - A List of parameters
     */
    def processRuntimeParameters(List<Parameter> p) {
        p.each {
            def matcher = it.getParameterValue() =~ /\{runtime:(.*)\}$/
            if (matcher.size() == 1) {
                def c = (new GroovyShell().evaluate("R = {${matcher[0][1]}}") as Closure).rehydrate(this, this, this)
                def newParam = c()
                it.setParameterValue(newParam)
            }
        }
    }

    /**
     * Process any parameters that have been ciphered by the Jasypt Wex plugin
     * @param p - The list of parameters
     */
    def processCipherParameters(List<Parameter> p) {
        p.each {
            def matcher = it.getParameterValue() =~ /\{cipher:(.*)\}$/
            if (matcher.size() == 1) {
                it.setParameterValue(decrypt(Base64.getDecoder().decode(matcher[0][1] as String)))
            }
        }
    }

    /**
     * Taken from Jasypt.
     * @todo enable Jasypt to have an API that allows the microdsl to access this function
     * @param encrypted - The byte representation of the encrypted String value
     * @return - Return the unencrypted value
     */
    def decrypt(byte[] encrypted) {
        if (encrypted[0] == 0xDF as byte && encrypted[1] == 0xBB as byte) {
            byte aesKeyNameLength = encrypted[2]
            byte ivLen = encrypted[3]

            int aesKeyNameOffset = 4
            int ivOffset = aesKeyNameOffset + aesKeyNameLength
            int dataOffset = ivOffset + ivLen

            String aesKeyName = new String(Arrays.copyOfRange(encrypted, aesKeyNameOffset, ivOffset), StandardCharsets.UTF_8)

            byte[] iv = Arrays.copyOfRange(encrypted, ivOffset, dataOffset)
            byte[] encryptedData = Arrays.copyOfRange(encrypted, dataOffset, encrypted.length)

            def aes =  AwsUtil.safeCall {
                getSsmClient().getParameter(new GetParameterRequest().withName(aesKeyName).withWithDecryption(true)).getParameter().getValue()
            }

            byte[] aesKeyValue = Base64.getDecoder().decode(aes.getBytes(StandardCharsets.UTF_8))
            Key aesKey = new SecretKeySpec(aesKeyValue, "AES")

            Cipher aesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
            aesCipher.init(Cipher.DECRYPT_MODE, aesKey, new IvParameterSpec(iv))

            new String(aesCipher.doFinal(encryptedData))
        } else throw new RuntimeException("Unable to decrypt parameter value: ${new String(encrypted)}")
    }

    def sync(TemplateSpec spec) {
        if (!region.getRegion(environment)) region = spec.region
        if (templatePath == DEFAULT_TEMPLATE && spec.templatePath != DEFAULT_TEMPLATE) templatePath = spec.templatePath
        if (!gChatSpec) {
            gChatSpec = spec.gChatSpec
            gChatSpec.templateSpec = this
        }
    }
}
