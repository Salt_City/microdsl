package com.wex.microdsl.deploy.aws

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient
import com.amazonaws.services.cloudformation.AmazonCloudFormationClientBuilder
import com.amazonaws.services.cloudformation.model.AlreadyExistsException
import com.amazonaws.services.cloudformation.model.Capability
import com.amazonaws.services.cloudformation.model.ChangeSetType
import com.amazonaws.services.cloudformation.model.CreateChangeSetRequest
import com.amazonaws.services.cloudformation.model.DescribeChangeSetRequest
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest
import com.amazonaws.services.cloudformation.model.ExecuteChangeSetRequest
import com.amazonaws.services.cloudformation.model.InsufficientCapabilitiesException
import com.amazonaws.services.cloudformation.model.Parameter
import com.amazonaws.services.cloudformation.model.Tag
import com.amazonaws.waiters.WaiterHandler
import com.amazonaws.waiters.WaiterParameters
import com.wex.microdsl.AwsUtil
import com.wex.microdsl.deploy.model.GChatSpec
import org.apache.log4j.LogManager
import org.apache.log4j.Logger

import javax.naming.LimitExceededException
import java.util.concurrent.CancellationException
import java.util.concurrent.ExecutionException
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Class with static functions to facilitate deployment to AWS
 */
class AwsDeploy {
    static Logger logger = LogManager.getLogger(AwsDeploy.class.getName())

    /**
     * Deploys a create or update CloudFormation request to AWS
     * @param stackName - The name of the stack
     * @param template - The CloudFormation YAML template as a String
     * @param params - The list of AWS CloudFormation paramaters
     * @param tags - The list of AWS CloudFormation Tags
     * @param region - The region to operation in (ie: us-east-1)
     * @param profile - The AWS credentials profile to apply to the AWS client calls
     * @return - Returns a boolean indicating whether the deploy was successful or not
     */
    public static boolean deployCreateOrUpdate(final String stackName, final String template, final List<Parameter> params,
                                        final List<Tag> tags, final String region, final String profile) {
        if (!region) {
            logger.error("AWS region not found. A region must be supplied")
            return false
        }
        def templateText
        try {
            templateText = new File(template).text
        } catch(e) {
            if (e instanceof FileNotFoundException) {
                logger.error("Template file was not found")
                logger.error(e.message)
                return false
            } else {
                logger.error("There was an error loading the template")
                logger.error(e.message)
                return false
            }
        }
        def cloudBuilder = AmazonCloudFormationClientBuilder.standard().withRegion(region)
        if(profile) cloudBuilder.setCredentials(new ProfileCredentialsProvider(profile))
        AmazonCloudFormationClient cloudClient = cloudBuilder.build()

        def id = "${stackName}-${System.currentTimeMillis()}"
        def request = new CreateChangeSetRequest()
                .withStackName(stackName)
                .withChangeSetName(id)
                .withTemplateBody(templateText)
                .withParameters(params)
                .withCapabilities(Capability.CAPABILITY_NAMED_IAM)
                .withTags(tags)
                .withChangeSetType(ChangeSetType.CREATE)

        logger.info("Creating Change Set request for ${stackName}... generally takes 10-20 seconds")

        if (!createChangeSet(cloudClient, request)) {
            return false
        } else {
            def changeSetWaiter = AwsUtil.safeCall {
                cloudClient.waiters().changeSetCreateComplete()
                        .runAsync(new WaiterParameters<>(new DescribeChangeSetRequest().withChangeSetName(id).withStackName(stackName)),
                                [onWaitSuccess: { input -> logger.info("Change Set request complete for ${stackName}") },
                                 onWaitFailure:{Exception e ->
                                     logger.error("Change request for ${stackName} failed")
                                     logger.error("Change request id: ${id}")
                                     logger.error("Exception message: ${e.getMessage()}")
                                     return false
                                 }] as WaiterHandler)
            }

            def changeSetResult = executeFuture(changeSetWaiter, 1)

            def describeChange = AwsUtil.safeCall {
                cloudClient.describeChangeSet(new DescribeChangeSetRequest()
                        .withStackName(stackName)
                        .withChangeSetName(id))
            }
            if (!changeSetResult) {
                if (describeChange.getChanges().isEmpty()) {
                    logger.info("No changes detected for ${stackName}")
                    return true
                } else {
                    logger.error("There was an error making the change request")
                    return false
                }
            }
            def stackHandler = [
                    onWaitSuccess: {req ->
                        logger.info("Deploy success")
                        return true
                    },
                    onWaitFailure: {Exception e ->
                        logger.error("Deploy failure")
                        logger.error("Exception message: ${e.getMessage()}")
                        return false
                    }
            ] as WaiterHandler

            logger.info("Executing deploy with these changes:")
            describeChange.getChanges().each{
                logger.info("${it.getResourceChange().getAction()}")
                logger.info(" => ${it.getResourceChange().getLogicalResourceId()}")
            }
            AwsUtil.safeCall {
                cloudClient.executeChangeSet(new ExecuteChangeSetRequest().withChangeSetName(id).withStackName(stackName))
            }

            def stackCreateWaiter = request.getChangeSetType() == ChangeSetType.CREATE.toString() ?
                    AwsUtil.safeCall{
                        cloudClient.waiters().stackCreateComplete()
                                .runAsync(new WaiterParameters<>(new DescribeStacksRequest().withStackName(stackName)),
                                        stackHandler)
                    }
                    :
                    AwsUtil.safeCall {
                        cloudClient.waiters().stackUpdateComplete()
                                .runAsync(new WaiterParameters<>(new DescribeStacksRequest().withStackName(stackName)),
                                        stackHandler)
                    }

            return executeFuture(stackCreateWaiter, 25)
        }
    }

    /**
     * Executes a blocking request to a future, waiting the specified time in minutes
     * @param request - The Future, expected to return void
     * @param timeout - The timeout in minutes
     * @return - Returns a boolean indicating whether the future executed or not
     */
    private static boolean executeFuture(Future<Void> request, int timeout) {
        try {
            request.get(timeout, TimeUnit.MINUTES)
            true
        } catch(e) {
            if (e instanceof CancellationException) {
                logger.error("ChangeSet request was canceled")
                logger.error(e.getMessage())
                false
            } else if (e instanceof ExecutionException) {
                logger.error("An error occurred while making the changeSet request")
                logger.error(e.getMessage())
                false
            } else if (e instanceof InterruptedException) {
                logger.error("ChangeSet request interrupted")
                logger.error(e.getMessage())
                false
            } else if (e instanceof TimeoutException) {
                logger.error("ChangeSet request timed out")
                false
            } else {
                logger.error("Error executing changeSet")
                logger.error(e.getMessage())
                false
            }
        }
    }

    /**
     * Create a CloudFormation change set request. This is expected to initially be called with a request type of
     *  CREATE, and the method will mutate the request with an UPDATE request type if a specific AWS error is thrown
     *  with a specfic message indicating that the CloudFormation stack being requested already exists
     * @param cloudClient - The CloudFormation client
     * @param request - The Change set request
     * @return - Returns a boolean indicating whether a change request completed successfully or not
     */
    private static boolean createChangeSet(AmazonCloudFormationClient cloudClient, CreateChangeSetRequest request) {
        try {
            AwsUtil.safeCall {
                cloudClient.createChangeSet(request)
            }
            true
        } catch (e) {
            if (e instanceof AlreadyExistsException) {
                logger.error("The changeSet with the name ${request.changeSetName} already exists")
                logger.error("AWS message: ${e.message}")
                false
            } else if (e instanceof InsufficientCapabilitiesException) {
                logger.error("The template contains capability requirements beyond what Microdsl can deploy with")
                logger.error("AWS message: ${e.getMessage()}")
                false
            } else if (e instanceof LimitExceededException) {
                logger.error("AWS API limit exceeded")
                logger.error("AWS message: ${e.getMessage()}")
                false
            } else {
                if (e.getMessage()
                        .contains("Stack [${request.stackName}] already exists and cannot be created again with the changeSet [${request.changeSetName}]")) {
                    logger.info("Service already exists, creating an update changeSet request")
                    request.setChangeSetType(ChangeSetType.UPDATE)
                    createChangeSet(cloudClient, request)
                } else {
                    logger.error(e.class.getName())
                    logger.error("Error creating changeSet")
                    logger.error("AWS message: ${e.getMessage()}")
                    false
                }

            }
        }
    }
}

