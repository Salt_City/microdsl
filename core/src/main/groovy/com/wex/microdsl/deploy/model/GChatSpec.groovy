package com.wex.microdsl.deploy.model

import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.POST

/**
 * Specification allowing the user to update a gchat space with any type of message based on the success or
 *  failure of a deployment
 */
class GChatSpec {
    String space
    String token
    TemplateSpec templateSpec
    Closure<String> success = {""}
    Closure<String> failure = {""}

    GChatSpec(TemplateSpec templateSpec) {
        this.templateSpec = templateSpec
    }

    def space(String space) {
        this.space = space
    }

    def token(String token) {
        this.token = token
    }

    def onSuccess(Closure<String> success) {
        this.success = success
    }

    def onFailure(Closure<String> failure) {
        this.failure = failure
    }

    def sendFailure() {
        send(this.failure.rehydrate(templateSpec, templateSpec, templateSpec).call())
    }

    def sendSuccess() {
        send(this.success.rehydrate(templateSpec, templateSpec, templateSpec).call())
    }

    def send(String message) {
        def url = "https://chat.googleapis.com/v1/spaces/${space}/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=${token}"
        new HTTPBuilder(url).request(POST) {
            requestContentType = JSON
            body = [ text: message ]
        }
    }
}
