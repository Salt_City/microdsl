package com.wex.microdsl.deploy.model

/**
 * Specification for template parameters
 */
class ParameterSpec {
    String key
    String value

    def key(String key) {
        this.key = key
    }

    def value(String value) {
        this.value = value
    }
}
