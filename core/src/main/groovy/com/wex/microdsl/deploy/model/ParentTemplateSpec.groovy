package com.wex.microdsl.deploy.model

/**
 * Parent template spec. Nothing special needed, but it does need its own class
 *  to differentiate it from a child. @see ChildTemplate
 */
class ParentTemplateSpec extends TemplateSpec {

}
