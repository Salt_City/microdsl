package com.wex.microdsl.deploy.model

/**
 * Specification for AWS profile credentials
 */
class ProfileSpec {
    HashMap<String, String> profiles = [:]

    def profile(String p) {
        def (env, name) = p.tokenize(":")
        profiles[env] = name
    }

    def getProfile(String env) {
        profiles[env]
    }
}
