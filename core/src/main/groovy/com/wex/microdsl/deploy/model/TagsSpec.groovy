package com.wex.microdsl.deploy.model

import com.amazonaws.services.cloudformation.model.Tag

/**
 * Spec for defining CloudFormation Tags
 */
class TagsSpec {
    String createdBy = " "
    String email = " "
    String lob = " "
    String compliance = " "
    String billing = " "
    String project = " "
    String purpose = " "
    String version = " "

    def createdBy(String createdBy) {
        this.createdBy = createdBy
    }

    def email(String email) {
        this.email = email
    }

    def lob(String lob) {
        this.lob = lob
    }

    def compliance(String compliance) {
        this.compliance = compliance
    }

    def billing(String billing) {
        this.billing = billing
    }

    def project(String project) {
        this.project = project
    }

    def purpose(String purpose) {
        this.purpose = purpose
    }

    def version(String version) {
        this.version = version
    }

    def toList() {
        [new Tag().withKey("CreatedBy").withValue(createdBy),
         new Tag().withKey("Email").withValue(email),
         new Tag().withKey("LOB").withValue(lob),
         new Tag().withKey("Compliance").withValue(compliance),
         new Tag().withKey("Billing").withValue(billing),
         new Tag().withKey("Project").withValue(project),
         new Tag().withKey("Purpose").withValue(purpose),
         new Tag().withKey("Version").withValue(version)]
    }
}
