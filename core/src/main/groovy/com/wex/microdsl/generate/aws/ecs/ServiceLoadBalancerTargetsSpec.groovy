package com.wex.microdsl.generate.aws.ecs

import com.wex.microdsl.generate.aws.BaseResourceSpec

class ServiceLoadBalancerTargetsSpec extends BaseResourceSpec {
    List<ServiceTargetSpec> targets = []

    def target(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServiceTargetSpec) Closure targetClosure) {
        def targetSpec = new ServiceTargetSpec()
        def t = targetClosure.rehydrate(targetSpec, this, this)
        t()
        targets << targetSpec
    }

    @Override
    String toString() {
        def tab = "    "
        def halfTab = "  "
        def builder = new StringBuilder().append("\n")
        targets.eachWithIndex{ t, i ->
            builder.append("${tab}${halfTab}").append("- ContainerName: ").append(t.containerName).append("\n")
                    .append("${tab}${tab}").append("ContainerPort: ").append(t.containerPort).append("\n")
                    .append("${tab}${tab}").append("TargetGroupArn: ").append(t.targetGroupArn).append("\n");
        }
        // delete that last newline
        return builder.deleteCharAt(builder.length()-1).toString();
    }

}
