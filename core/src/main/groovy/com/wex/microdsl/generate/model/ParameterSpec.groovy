package com.wex.microdsl.generate.model

/**
 * AWS template Parameter spec. Only Name and Type are required attributes
 */
class ParameterSpec {
    String name
    String type
    String description
    String defaultValue
    String noEcho

    // Required
    def name(String name) {
        this.name = name
    }

    // Required
    def type(String type) {
        this.type = type
    }

    def description(String description) {
        this.description = description
    }

    def defaultValue(String defaultValue) {
        this.defaultValue = defaultValue
    }

    def noEcho(String echo) {
        this.noEcho = echo
    }
}
