package com.wex.microdsl.generate.aws.elasticloadbalancingv2

import com.wex.microdsl.generate.aws.BaseResourceSpec

class ListenerRuleSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def targetGroupArn(String targetGroupArn) {
		binding["targetGroupArn"] = targetGroupArn
	}

	def pathPattern(String pathPattern) {
		binding["pathPattern"] = pathPattern
	}

	def listenerImport(String listenerImport) {
		binding["listenerImport"] = listenerImport
	}

	def priority(String priority) {
		binding["priority"] = priority
	}

}