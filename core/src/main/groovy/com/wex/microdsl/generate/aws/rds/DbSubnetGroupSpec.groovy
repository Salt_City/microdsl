package com.wex.microdsl.generate.aws.rds

import com.wex.microdsl.generate.aws.BaseResourceSpec

class DbSubnetGroupSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def subnets(String subnets) {
		binding["subnets"] = subnets
	}

}