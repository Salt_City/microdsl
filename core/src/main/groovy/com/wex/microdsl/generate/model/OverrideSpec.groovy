package com.wex.microdsl.generate.model

/**
 * An Override component is very similar to the concept of `extend` in an OO language. The component will inherit
 *  all resources from the component it is extending, and it also has the option of overriding a resource defined in the parent,
 *  along with adding any amount of parameters, and deleting any defined parameters in the parent.
 *
 *  An Override component can only override ONE resource in the component it is inheriting from. This was a deliberate design
 *   choice, as it felt like an anti-pattern could develop if you were to allow people to make hideous components that override
 *   others and completely mangled them. The thought here is that if you have two similar components, but they are different
 *   enough to need more than one different resource, go ahead and make a standalone component
 */
class OverrideSpec extends ComponentSpec implements LazyComponent {
    ComponentSpec selectedComponent
    ComponentSpec overriddenComponent
    String selectedResource
    String overrideResource
    List<String> paramsToRemove = []

    OverrideSpec(List<ComponentSpec> components) {
        super(components)
    }

    def impl(String ref) {
        this.selectedComponent = components.find{it.name == ref}
        this.parametersSpec.parameters += selectedComponent.parametersSpec.parameters
        this.ec2Spec.consume(selectedComponent.ec2Spec)
        this.ecsSpec.consume(selectedComponent.ecsSpec)
        this.elasticloadbalancingv2Spec.consume(selectedComponent.elasticloadbalancingv2Spec)
        this.rdsSpec.consume(selectedComponent.rdsSpec)
        this.route53Spec.consume(selectedComponent.route53Spec)
        this.lazyComponents += selectedComponent.lazyComponents
        // go ahead and add this to the list of lazy's to be init'd since it is a lazy component and owns itself
        this.lazyComponents << this
    }

    def override(String ref) {
        selectedResource = ref
    }

    def to(String ref) {
        overrideResource = ref
    }

    def removeParams(String ps) {
        paramsToRemove = ps.split(",")
    }

    def resourceDefinition(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ComponentSpec) Closure componentClosure) {
        overriddenComponent = new ComponentSpec(components)
        def c = componentClosure.rehydrate(overriddenComponent, this, overriddenComponent)
        c()
    }

    @Override
    def init() {
        this.parametersSpec.parameters += overriddenComponent.parametersSpec.parameters
        paramsToRemove.each{p -> this.parametersSpec.parameters.remove(this.parametersSpec.parameters.find{it.name == p})}
        def (spec, resource) = selectedResource.tokenize("::")
        def (oSpec, oResource) = overrideResource.tokenize("::")
        def currentResources = this.getSpecByName(spec).getResourceByName(resource)
        currentResources.clear()
        currentResources.addAll(overriddenComponent.getSpecByName(oSpec).getResourceByName(oResource))
    }
}
