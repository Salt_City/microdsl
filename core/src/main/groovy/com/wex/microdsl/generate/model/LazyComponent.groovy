package com.wex.microdsl.generate.model

/**
 * A lazy component is a component that will not initialize until it is time to generate the AWS template.
 * This is necessary because when a child inherits from a parent, ALL components defined in the parent are consumed by
 * the DSL parser. It is not a guarantee though that the child will invoke all of the components that the parent has defined,
 * in fact it is actually almost a guarantee that the child will always invoke only a subset of the components defined by
 * the parent. So we have parsed through the parent and have all the components defined, and they have direct references to
 * the base component that most of them are going to insert, extend, or override, but we don't want them to make those changes
 * unless they are actually called on by the child. The solution was then to only initialize the component when directly
 * called on by the child template during the AWS template generation.
 *
 * @see ChildGenerateSpec#generate for the actual call to the `init` function on the lazy componet
 *
 * As a pattern, all possible mutations to other components defined should ONLY exist in the init function
 *
 */
interface LazyComponent {
    def init();
}