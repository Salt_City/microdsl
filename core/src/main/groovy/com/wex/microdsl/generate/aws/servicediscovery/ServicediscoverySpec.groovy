package com.wex.microdsl.generate.aws.servicediscovery

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class ServicediscoverySpec implements BaseSpec {

	List<ServiceSpec> services = []


	def service(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServiceSpec) Closure serviceClosure) {
		def specification = new ServiceSpec()
		def c = serviceClosure.rehydrate(specification, this, this)
		c()
		services << specification
	}

	@Override
	def getResources() {
		[services]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof ServicediscoverySpec
		services += sub.services
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "service" :
				services
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		services.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}