package com.wex.microdsl.generate.model

/**
 * Parent generate Specification
 */
class ParentGenerateSpec extends GenerateSpec {

    def getDefinedComponent(String ref) {
        components.find{it.name == ref}
    }
}
