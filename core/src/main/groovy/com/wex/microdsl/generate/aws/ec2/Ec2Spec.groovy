package com.wex.microdsl.generate.aws.ec2

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class Ec2Spec implements BaseSpec {

	List<SecurityGroupIngressSpec> securitygroupingresss = []
	List<SecurityGroupSpec> securitygroups = []


	def securityGroupIngress(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = SecurityGroupIngressSpec) Closure securitygroupingressClosure) {
		def specification = new SecurityGroupIngressSpec()
		def c = securitygroupingressClosure.rehydrate(specification, this, this)
		c()
		securitygroupingresss << specification
	}

	def securityGroup(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = SecurityGroupSpec) Closure securitygroupClosure) {
		def specification = new SecurityGroupSpec()
		def c = securitygroupClosure.rehydrate(specification, this, this)
		c()
		securitygroups << specification
	}

	@Override
	def getResources() {
		[securitygroupingresss,
		securitygroups]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof Ec2Spec
		securitygroupingresss += sub.securitygroupingresss
		securitygroups += sub.securitygroups
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "securitygroupingress" :
				securitygroupingresss
				break
			case "securitygroup" :
				securitygroups
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		securitygroupingresss.each{builder.append(it.generate()).append("\n")}
		securitygroups.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}