package com.wex.microdsl.generate.aws.route53

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class Route53Spec implements BaseSpec {

	List<RecordSetSpec> recordsets = []


	def recordSet(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RecordSetSpec) Closure recordsetClosure) {
		def specification = new RecordSetSpec()
		def c = recordsetClosure.rehydrate(specification, this, this)
		c()
		recordsets << specification
	}

	@Override
	def getResources() {
		[recordsets]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof Route53Spec
		recordsets += sub.recordsets
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "recordset" :
				recordsets
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		recordsets.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}