package com.wex.microdsl.generate.aws.ecs

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class EcsSpec implements BaseSpec {

	List<ServiceSpec> services = []
	List<TaskDefinitionSpec> taskdefinitions = []


	def service(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServiceSpec) Closure serviceClosure) {
		def specification = new ServiceSpec()
		def c = serviceClosure.rehydrate(specification, this, this)
		c()
		services << specification
	}

	def taskDefinition(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = TaskDefinitionSpec) Closure taskdefinitionClosure) {
		def specification = new TaskDefinitionSpec()
		def c = taskdefinitionClosure.rehydrate(specification, this, this)
		c()
		taskdefinitions << specification
	}

	@Override
	def getResources() {
		[services,
		taskdefinitions]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof EcsSpec
		services += sub.services
		taskdefinitions += sub.taskdefinitions
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "service" :
				services
				break
			case "taskdefinition" :
				taskdefinitions
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		services.each{builder.append(it.generate()).append("\n")}
		taskdefinitions.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}