package com.wex.microdsl.generate.model

import com.amazonaws.services.cloudformation.model.Output
import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.ec2.Ec2Spec
import com.wex.microdsl.generate.aws.ecs.EcsSpec
import com.wex.microdsl.generate.aws.elasticloadbalancingv2.Elasticloadbalancingv2Spec
import com.wex.microdsl.generate.aws.rds.RdsSpec
import com.wex.microdsl.generate.aws.route53.Route53Spec
import com.wex.microdsl.generate.aws.servicediscovery.ServicediscoverySpec

/**
 * Specification for an AWS component defined in the microdsl file
 */
class ComponentSpec {
    List<ComponentSpec> components
    String name
    ParametersSpec parametersSpec = new ParametersSpec()
    EcsSpec ecsSpec = new EcsSpec()
    Ec2Spec ec2Spec = new Ec2Spec()
    Elasticloadbalancingv2Spec elasticloadbalancingv2Spec = new Elasticloadbalancingv2Spec()
    ServicediscoverySpec servicediscoverySpec = new ServicediscoverySpec()
    RdsSpec rdsSpec = new RdsSpec()
    Route53Spec route53Spec = new Route53Spec()
    List<LazyComponent> lazyComponents = []
    List<OutputSpec> outputs = []

    /**
     * Components keep a reference to the collection they are contained in. This is because a component can contain
     *  references to several other components that may need to directly mutate other defined components. an example
     *  would be a component that extends the `standard` component defined in the parent microdsl file for the microdsl
     *  team. This component might need to add a target to the ECS service defined in the standard component, and in order
     *  to do that it will need the reference to that component.
     *
     *  Components follow a similar pattern as a DockerFile, where you can define layers and build them on top of each other.
     *  Consider this when defining components, as order matters, and the way in which components are crafted should have
     *  this pattern in mind.
     * @param components - The list of components injected from the owner
     */
    ComponentSpec(List<ComponentSpec> components) {
        this.components = components
    }

    def name(String name) {
        this.name = name
    }

    def extend(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ExtendSpec) Closure extendClosure) {
        def extendSpec = new ExtendSpec(components)
        def e = extendClosure.rehydrate(extendSpec, this, extendSpec)
        e()
        lazyComponents << extendSpec
    }

    def insert(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = InsertSpec) Closure insertClosure) {
        def insertSpec = new InsertSpec(components)
        def e = insertClosure.rehydrate(insertSpec, this, insertSpec)
        e()
        lazyComponents << insertSpec
    }

    def parameters(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ParametersSpec) Closure paramClosure) {
        def e = paramClosure.rehydrate(parametersSpec, this, this)
        e()
    }

    def ecs(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EcsSpec) Closure ecsClosure) {
        def e = ecsClosure.rehydrate(ecsSpec, this, this)
        e()
    }

    def ec2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Ec2Spec) Closure ec2Closure) {
        def e = ec2Closure.rehydrate(ec2Spec, this, this)
        e()
    }

    def elbV2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Elasticloadbalancingv2Spec) Closure elbClosure) {
        def e = elbClosure.rehydrate(elasticloadbalancingv2Spec, this, this)
        e()
    }

    def serviceDiscovery(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServicediscoverySpec) Closure serviceClosure) {
        def e = serviceClosure.rehydrate(servicediscoverySpec, this, this)
        e()
    }

    def rds(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RdsSpec) Closure rdsClosure) {
        def e = rdsClosure.rehydrate(rdsSpec, this, this)
        e()
    }

    def route53(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Route53Spec) Closure route53Closure) {
        def e = route53Closure.rehydrate(route53Spec, this, this)
        e()
    }

    def outputs(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = OutputSpec) Closure outputClosure) {
        def o = new OutputSpec()
        def e = outputClosure.rehydrate(o, o, o)
        e()
        outputs << o
    }

    /**
     * Generates all the resources defined in this component
     * @return - Returns a String with all resources
     */
    def generate() {
        def builder = new StringBuilder()
        builder.append(ecsSpec.generate()).append("\n")
                .append(ec2Spec.generate()).append("\n")
                .append(elasticloadbalancingv2Spec.generate()).append("\n")
                .append(servicediscoverySpec.generate()).append("\n")
                .append(rdsSpec.generate()).append("\n")
                .append(route53Spec.generate()).append("\n")
                .toString()
    }

    /**
     * Get a spec list based on the common name
     * @param name - The common name that represents the AWS resource
     * @return - Returns a list of BaseSpecs
     */
    BaseSpec getSpecByName(String name) {
        switch(name) {
            case "ecs":
                ecsSpec
                break
            case "ec2":
                ec2Spec
                break
            case "elbv2":
                elasticloadbalancingv2Spec
                break
            case "serviceDiscovery":
                servicediscoverySpec
                break
            case "rds":
                rdsSpec
                break
            case "route53":
                route53Spec
                break
            default :
                null
        }
    }
}
