package com.wex.microdsl.generate.model

/**
 * Base specification for the Generate closure in the Microdsl file provided to this program.
 */
class GenerateSpec {
    List<ComponentSpec> components = []
    List<String> componentReferences = []
    String description

    def description(String description) {
        this.description = description
    }

    def component(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ComponentSpec) Closure componentClosure) {
        def spec = new ComponentSpec(components)
        def c = componentClosure.rehydrate(spec, this, this)
        c()
        components << spec
    }

    def overrideComponent(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = OverrideSpec) Closure override) {
        def over = new OverrideSpec(components)
        def c = override.rehydrate(over, this, this)
        c()
        components << over
    }
}
