package com.wex.microdsl.generate.model

class OutputSpec {
    String name
    String description
    String value
    String exportName

    def name(String name) {
        this.name = name
    }

    def description(String description) {
        this.description = description
    }

    def value(String value) {
        this.value = value
    }

    def exportName(String exportName) {
        this.exportName = exportName
    }

    def generate() {
        def builder = new StringBuilder()
        def tab = "    "
        def halfTab = "  "

        builder.append(halfTab).append("${name}:").append("\n")
            .append(tab).append("Description: ").append(description).append("\n")
            .append(tab).append("Value: ").append(value).append("\n")
            .append(tab).append("Export: ").append("\n").append(tab).append(halfTab).append("Name: ").append(exportName)
            .append("\n")

        builder.toString()
    }
}
