package com.wex.microdsl.generate.aws.rds

import com.wex.microdsl.generate.aws.BaseResourceSpec

class DbClusterSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def backupRetention(String backupRetention) {
		binding["backupRetention"] = backupRetention
	}

	def deletionProtection(String deletionProtection) {
		binding["deletionProtection"] = deletionProtection
	}

	def username(String username) {
		binding["username"] = username
	}

	def password(String password) {
		binding["password"] = password
	}

	def dbName(String dbName) {
		binding["dbName"] = dbName
	}

	def engine(String engine) {
		binding["engine"] = engine
	}

	def maxCapacity(String maxCapacity) {
		binding["maxCapacity"] = maxCapacity
	}

	def minCapacity(String minCapacity) {
		binding["minCapacity"] = minCapacity
	}

	def autoPauseTime(String autoPauseTime) {
		binding["autoPauseTime"] = autoPauseTime
	}

	def subnetGroup(String subnetGroup) {
		binding["subnetGroup"] = subnetGroup
	}

	def vpcGroupId(String vpcGroupId) {
		binding["vpcGroupId"] = vpcGroupId
	}

}