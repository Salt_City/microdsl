package com.wex.microdsl.generate.aws.servicediscovery

import com.wex.microdsl.generate.aws.BaseResourceSpec

class ServiceSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def serviceName(String serviceName) {
		binding["serviceName"] = serviceName
	}

	def namespaceId(String namespaceId) {
		binding["namespaceId"] = namespaceId
	}

}