package com.wex.microdsl.generate.model

import com.wex.microdsl.generate.aws.BaseResourceSpec
import com.wex.microdsl.generate.aws.ec2.Ec2Spec
import com.wex.microdsl.generate.aws.ecs.EcsSpec
import com.wex.microdsl.generate.aws.elasticloadbalancingv2.Elasticloadbalancingv2Spec
import com.wex.microdsl.generate.aws.rds.RdsSpec
import com.wex.microdsl.generate.aws.route53.Route53Spec
import com.wex.microdsl.generate.aws.servicediscovery.ServicediscoverySpec

/**
 * Extension component. An extension component is used by other components to extend base components that they are
 *  built off of. Easy example is the `external` component in the Microservice parent microdsl file. This component
 *  is building off of the `standard` base component defined, by simply adding a target group and listener rule to the
 *  external ALB in our core. The ECS Service defined in the `standard` component needs to be aware of this new external
 *  target group, so the external component `extends` the standard, and defines a target group definition inside of the
 *  ECS Service resource, which is then injected into the standard component when the template is generated.
 */
class ExtendSpec extends ComponentSpec implements LazyComponent {
    ComponentSpec selectedComponent
    boolean override = false
    def extendedComponents = []

    ExtendSpec(List<ComponentSpec> components) {
        super(components)
    }

    def override(boolean override) {
        this.override = override
    }

    @Override
    def ecs(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EcsSpec) Closure ecsClosure) {
        super.ecs(ecsClosure)
        extendedComponents << new Tuple(selectedComponent.ecsSpec, ecsSpec)
    }

    @Override
    def ec2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Ec2Spec) Closure ec2Closure) {
        super.ec2(ec2Closure)
        extendedComponents << new Tuple(selectedComponent.ec2Spec, ec2Spec)
    }

    @Override
    def elbV2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Elasticloadbalancingv2Spec) Closure elbClosure) {
        super.elbV2(elbClosure)
        extendedComponents << new Tuple(selectedComponent.elasticloadbalancingv2Spec, elasticloadbalancingv2Spec)
    }

    @Override
    def serviceDiscovery(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServicediscoverySpec) Closure serviceClosure) {
        super.serviceDiscovery(serviceClosure)
        extendedComponents << new Tuple(selectedComponent.servicediscoverySpec, servicediscoverySpec)
    }

    @Override
    def rds(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RdsSpec) Closure rdsClosure) {
        super.rds(rdsClosure)
        extendedComponents << new Tuple(selectedComponent.rdsSpec, rdsSpec)
    }

    @Override
    def route53(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Route53Spec) Closure route53Closure) {
        super.route53(route53Closure)
        extendedComponents << new Tuple(selectedComponent.route53Spec, route53Spec)
    }

    def component(String ref) {
        this.selectedComponent = components.find{it.name == ref}
    }

    @Override
    def init() {
        extendedComponents.each{extendBinding(it[0].getResources(), it[1].getResources())}
    }

    /**
     * Extends the binding of the selected component. Either adds to the binding, or completely overrides it based on
     *  the override flag provided (defaults to false).
     * @param component - The selected component
     * @param extension - The desired extension
     */
    private def extendBinding(List component, List extension) {
        def zipList = []
        component.eachWithIndex{ entry, i ->
            zipList += [entry, extension[i]].transpose()
        }

        zipList.each { BaseResourceSpec a, BaseResourceSpec b ->
            b.binding.keySet().each {
                if (override) a.binding[it] = b.binding[it]
                else a.binding[it] += b.binding[it]
            }
        }
    }


}
