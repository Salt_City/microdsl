package com.wex.microdsl.generate.aws.ecs

class ServiceTargetSpec {
    String containerName
    String containerPort
    String targetGroupArn

    def containerName(String containerName) {
        this.containerName = containerName
    }

    def containerPort(String containerPort) {
        this.containerPort = containerPort
    }

    def targetGroupArn(String targetGroupArn) {
        this.targetGroupArn = targetGroupArn
    }
}
