package com.wex.microdsl.generate.aws

/**
 * Interface that serves as a base for all top level AWS specifications
 */
interface BaseSpec {

    def getResources();
    def <T extends BaseSpec> void consume(T sub);
    List<BaseResourceSpec> getResourceByName(String name);
    def generate();
}
