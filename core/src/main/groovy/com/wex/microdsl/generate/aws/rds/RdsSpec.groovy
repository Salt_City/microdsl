package com.wex.microdsl.generate.aws.rds

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class RdsSpec implements BaseSpec {

	List<DbClusterSpec> dbclusters = []
	List<DbSubnetGroupSpec> dbsubnetgroups = []
	List<DbClusterSnapshotSpec> dbclustersnapshots = []


	def dbCluster(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = DbClusterSpec) Closure dbclusterClosure) {
		def specification = new DbClusterSpec()
		def c = dbclusterClosure.rehydrate(specification, this, this)
		c()
		dbclusters << specification
	}

	def dbSubnetGroup(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = DbSubnetGroupSpec) Closure dbsubnetgroupClosure) {
		def specification = new DbSubnetGroupSpec()
		def c = dbsubnetgroupClosure.rehydrate(specification, this, this)
		c()
		dbsubnetgroups << specification
	}

	def dbClusterSnapshot(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = DbClusterSnapshotSpec) Closure dbclustersnapshotClosure) {
		def specification = new DbClusterSnapshotSpec()
		def c = dbclustersnapshotClosure.rehydrate(specification, this, this)
		c()
		dbclustersnapshots << specification
	}

	@Override
	def getResources() {
		[dbclusters,
		dbsubnetgroups,
		dbclustersnapshots]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof RdsSpec
		dbclusters += sub.dbclusters
		dbsubnetgroups += sub.dbsubnetgroups
		dbclustersnapshots += sub.dbclustersnapshots
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "dbcluster" :
				dbclusters
				break
			case "dbsubnetgroup" :
				dbsubnetgroups
				break
			case "dbclustersnapshot" :
				dbclustersnapshots
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		dbclusters.each{builder.append(it.generate()).append("\n")}
		dbsubnetgroups.each{builder.append(it.generate()).append("\n")}
		dbclustersnapshots.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}