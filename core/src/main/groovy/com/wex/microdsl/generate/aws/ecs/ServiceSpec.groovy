package com.wex.microdsl.generate.aws.ecs

import com.wex.microdsl.generate.aws.BaseResourceSpec

class ServiceSpec extends BaseResourceSpec {

    def resourceName(String resourceName) {
        binding["resourceName"] = resourceName
    }

    def cluster(String cluster) {
        binding["cluster"] = cluster
    }

    def desiredCount(String desiredCount) {
        binding["desiredCount"] = desiredCount
    }

    def taskDefinition(String taskDefinition) {
        binding["taskDefinition"] = taskDefinition
    }

    def ecsNodeGroup(String ecsNodeGroup) {
        binding["ecsNodeGroup"] = ecsNodeGroup
    }

    def serviceSg(String serviceSg) {
        binding["serviceSg"] = serviceSg
    }

    def subnets(String subnets) {
        binding["subnets"] = subnets
    }

    def targets(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServiceLoadBalancerTargetsSpec) Closure targetClosure) {
        def targets = new ServiceLoadBalancerTargetsSpec()
        def t = targetClosure.rehydrate(targets, this, this)
        t()
        binding["targets"] = targets.toString()
    }

    def serviceDiscovery(String serviceDiscovery) {
        binding["serviceDiscovery"] = serviceDiscovery
    }

    def tagName(String tagName) {
        binding["tagName"] = tagName
    }
}
