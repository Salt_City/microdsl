package com.wex.microdsl.generate.aws.ec2

import com.wex.microdsl.generate.aws.BaseResourceSpec

class SecurityGroupSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def vpc(String vpc) {
		binding["vpc"] = vpc
	}

	def tagName(String tagName) {
		binding["tagName"] = tagName
	}

}