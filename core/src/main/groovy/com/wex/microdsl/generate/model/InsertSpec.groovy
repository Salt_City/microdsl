package com.wex.microdsl.generate.model

import com.wex.microdsl.generate.aws.ec2.Ec2Spec
import com.wex.microdsl.generate.aws.ecs.EcsSpec
import com.wex.microdsl.generate.aws.elasticloadbalancingv2.Elasticloadbalancingv2Spec
import com.wex.microdsl.generate.aws.rds.RdsSpec
import com.wex.microdsl.generate.aws.route53.Route53Spec
import com.wex.microdsl.generate.aws.servicediscovery.ServicediscoverySpec

/**
 * Insert Specification. The Insert specification is used by a component that would like to insert a new resource into
 *  an already existing component.
 *
 *  Consider again that Components follow the `layering` pattern, similar to a DockerFile. Some components defined after
 *  the `base` component may want to mutate certain aspects of the base. An example would be a database that would like to
 *  `insert` a security group ingress into the the service component definition.
 */
class InsertSpec extends ComponentSpec implements LazyComponent {
    ComponentSpec selectedComponent
    def insertedComponents = []

    InsertSpec(List<ComponentSpec> components) {
        super(components)
    }

    def into(String ref) {
        this.selectedComponent = components.find{it.name == ref}
    }

    @Override
    def ecs(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = EcsSpec) Closure ecsClosure) {
        super.ecs(ecsClosure)
        insertedComponents << new Tuple(selectedComponent.ecsSpec, ecsSpec)
    }

    @Override
    def ec2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Ec2Spec) Closure ec2Closure) {
        super.ec2(ec2Closure)
        insertedComponents << new Tuple(selectedComponent.ec2Spec, ec2Spec)
    }

    @Override
    def elbV2(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Elasticloadbalancingv2Spec) Closure elbClosure) {
        super.elbV2(elbClosure)
        insertedComponents << new Tuple(selectedComponent.elasticloadbalancingv2Spec, elasticloadbalancingv2Spec)
    }

    @Override
    def serviceDiscovery(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ServicediscoverySpec) Closure serviceClosure) {
        super.serviceDiscovery(serviceClosure)
        insertedComponents << new Tuple(selectedComponent.servicediscoverySpec, servicediscoverySpec)
    }

    @Override
    def rds(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = RdsSpec) Closure rdsClosure) {
        super.rds(rdsClosure)
        insertedComponents << new Tuple(selectedComponent.rdsSpec, rdsSpec)
    }

    @Override
    def route53(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = Route53Spec) Closure route53Closure) {
        super.route53(route53Closure)
        insertedComponents << new Tuple(selectedComponent.route53Spec, route53Spec)
    }

    @Override
    def init() {
        selectedComponent.parametersSpec.parameters += parametersSpec.parameters
        insertedComponents.each{it[0].consume(it[1])}
    }
}
