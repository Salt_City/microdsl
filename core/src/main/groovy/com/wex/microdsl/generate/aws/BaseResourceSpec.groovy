package com.wex.microdsl.generate.aws

import com.wex.microdsl.Microdsl
import com.wex.microdsl.model.DeploySpec
import groovy.text.SimpleTemplateEngine

/**
 * Base AWS resource class
 */
abstract class BaseResourceSpec {
    def module = getClass().getPackage().getName().replace("com.wex.microdsl.generate.aws.", "")
    def path = "/templates/service/${module}"
    def name = "${getClass().getName().replace("com.wex.microdsl.generate.aws.${module}.", "").replace("Spec", "")}.yml"
    def resource = getClass().getResource(path + "/${name}")
    def binding = [:]
    def template = new SimpleTemplateEngine()
    String team = DeploySpec.team
    String stackName = DeploySpec.stackName
    String serviceName = DeploySpec.serviceName
    String environment = Microdsl.environment

    def getConfig(String name, String key) {
        DeploySpec.configMappings[name][key]
    }

    def findMavenProperty(String key) {
        Microdsl.project.getProperties().get(key)
    }

    def getMavenVersion() {
        Microdsl.project.getVersion()
    }

    def generate() {
        def halfTab = "  "
        halfTab + template.createTemplate(resource).make(binding).toString().replace("\n", "\n${halfTab}")
    }

}
