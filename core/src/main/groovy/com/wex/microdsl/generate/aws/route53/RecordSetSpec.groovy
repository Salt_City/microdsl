package com.wex.microdsl.generate.aws.route53

import com.wex.microdsl.generate.aws.BaseResourceSpec

class RecordSetSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def hostedZoneId(String hostedZoneId) {
		binding["hostedZoneId"] = hostedZoneId
	}

	def resourceRecords(String resourceRecords) {
		binding["resourceRecords"] = resourceRecords
	}

	def recordEndPoint(String recordEndPoint) {
		binding["recordEndPoint"] = recordEndPoint
	}

}