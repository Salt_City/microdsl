package com.wex.microdsl.generate.aws.ecs

import com.wex.microdsl.generate.aws.BaseResourceSpec

class TaskDefinitionSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def cpu(String cpu) {
		binding["cpu"] = cpu
	}

	def memory(String memory) {
		binding["memory"] = memory
	}

	def executionRoleArn(String executionRoleArn) {
		binding["executionRoleArn"] = executionRoleArn
	}

	def taskRoleArn(String taskRoleArn) {
		binding["taskRoleArn"] = taskRoleArn
	}

	def contextPath(String contextPath) {
		binding["contextPath"] = contextPath
	}

	def springProfile(String springProfile) {
		binding["springProfile"] = springProfile
	}

	def namespace(String namespace) {
		binding["namespace"] = namespace
	}

	def serviceName(String serviceName) {
		binding["serviceName"] = serviceName
	}

	def lob(String lob) {
		binding["lob"] = lob
	}

	def tag(String tag) {
		binding["tag"] = tag
	}

	def logGroup(String logGroup) {
		binding["logGroup"] = logGroup
	}

}