package com.wex.microdsl.generate.model

/**
 * The child representation of the Generate closure. @see ChildTemplate for a more detailed explanation of why
 *  there is a distinction between a child and parent
 *  @todo need to separate the model from the impl
 */
class ChildGenerateSpec extends GenerateSpec {
    ParentGenerateSpec parent = new ParentGenerateSpec()

    def setParent(ParentGenerateSpec parent) {
        this.parent = parent
    }

    /**
     * @todo probably can get rid of these component reference lists
     */
    def component(String reference) {
        componentReferences << reference
        components << parent.getDefinedComponent(reference)
    }

    /**
     * Function for generating the actual AWS template defined in the microdsl file provided to the program.
     *  Prior to dumping out all the template data, the method will go through and invoke any lazy components that
     *  are contained in the components provided to this method
     * @param c - The list of components to add to the template
     * @return returns a String representation of an AWS template
     */
    def generate(List<? extends ComponentSpec> c) {
        c.each{ it.lazyComponents.each{ it.init() } }
        def builder = new StringBuilder()

        builder.append("AWSTemplateFormatVersion: 2010-09-09").append("\n")
            .append("Description: ${description}").append("\n")
        def params = c.collect{it.parametersSpec}
        if(params.find{it.parameters.size() > 0}) {
            builder.append("Parameters:").append("\n")
            params.each{
                builder.append(it.generate())
            }
        }
        builder.append("Resources:").append("\n")
        c.each {
            builder.append(it.generate())
        }
        def outputs = c.collect{it.outputs}
        if (outputs.find{it.size() > 0}) {
            builder.append("Outputs:").append("\n")
            outputs.each {
                it.each {
                    builder.append(it.generate())
                }
            }
        }
        builder.toString()
    }

    def generateService() {
        generate(components)
    }
}
