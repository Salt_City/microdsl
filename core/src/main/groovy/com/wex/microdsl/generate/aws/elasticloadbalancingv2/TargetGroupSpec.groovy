package com.wex.microdsl.generate.aws.elasticloadbalancingv2

import com.wex.microdsl.generate.aws.BaseResourceSpec

class TargetGroupSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def port(String port) {
		binding["port"] = port
	}

	def vpc(String vpc) {
		binding["vpc"] = vpc
	}

	def tagName(String tagName) {
		binding["tagName"] = tagName
	}

}