package com.wex.microdsl.generate.aws.elasticloadbalancingv2

import com.wex.microdsl.generate.aws.BaseSpec
import com.wex.microdsl.generate.aws.BaseResourceSpec

class Elasticloadbalancingv2Spec implements BaseSpec {

	List<ListenerRuleSpec> listenerrules = []
	List<TargetGroupSpec> targetgroups = []


	def listenerRule(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ListenerRuleSpec) Closure listenerruleClosure) {
		def specification = new ListenerRuleSpec()
		def c = listenerruleClosure.rehydrate(specification, this, this)
		c()
		listenerrules << specification
	}

	def targetGroup(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = TargetGroupSpec) Closure targetgroupClosure) {
		def specification = new TargetGroupSpec()
		def c = targetgroupClosure.rehydrate(specification, this, this)
		c()
		targetgroups << specification
	}

	@Override
	def getResources() {
		[listenerrules,
		targetgroups]
	}

	@Override
	def <T extends BaseSpec> void consume(T sub) {
		assert sub instanceof Elasticloadbalancingv2Spec
		listenerrules += sub.listenerrules
		targetgroups += sub.targetgroups
	}

	@Override
	List<BaseResourceSpec> getResourceByName(String name) {
		switch (name) {
			case "listenerrule" :
				listenerrules
				break
			case "targetgroup" :
				targetgroups
				break
			default :
				null
		}
	}

	@Override
	def generate() {
		def builder = new StringBuilder()

		listenerrules.each{builder.append(it.generate()).append("\n")}
		targetgroups.each{builder.append(it.generate()).append("\n")}
		builder.toString()
	}
}