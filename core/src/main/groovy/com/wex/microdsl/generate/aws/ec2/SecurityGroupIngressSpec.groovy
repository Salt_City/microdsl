package com.wex.microdsl.generate.aws.ec2

import com.wex.microdsl.generate.aws.BaseResourceSpec

class SecurityGroupIngressSpec extends BaseResourceSpec {

	def resourceName(String resourceName) {
		binding["resourceName"] = resourceName
	}

	def resource(String resource) {
		binding["resource"] = resource
	}

	def resourceGroupId(String resourceGroupId) {
		binding["resourceGroupId"] = resourceGroupId
	}

	def fromPort(String fromPort) {
		binding["fromPort"] = fromPort
	}

	def toPort(String toPort) {
		binding["toPort"] = toPort
	}

	def sourceSecurityGroup(String sourceSecurityGroup) {
		binding["sourceSecurityGroup"] = sourceSecurityGroup
	}

}