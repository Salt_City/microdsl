package com.wex.microdsl.generate.model

/**
 * The parent Spec that holds all defined AWS template parameters
 * @todo separate model from impl
 */
class ParametersSpec {
    List<ParameterSpec> parameters = []

    def parameter(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ParameterSpec) Closure paramClosure) {
        def paramSpec = new ParameterSpec()
        def p = paramClosure.rehydrate(paramSpec, this, this)
        p()
        parameters << paramSpec
    }

    /**
     * Generate function generates AWS template parameters for insertion into an AWS template
     * @return a String of formatted AWS template parameters
     */
    def generate() {
        if (parameters.empty) ""
        else {
            def builder = new StringBuilder()
            def tab = "    "
            def halfTab = "  "
            parameters.each {
                builder.append(halfTab).append("${it.name}:").append("\n")
                        .append("${tab}").append("Type: ").append(it.type).append("\n")
                if (it.description) builder.append("${tab}").append("Description: ").append(it.description).append("\n")
                if (it.defaultValue) builder.append("${tab}").append("Default: ").append(it.defaultValue).append("\n")
                if (it.noEcho) builder.append("${tab}").append("NoEcho: ").append(it.noEcho).append("\n")
            }
            builder.toString()
        }
    }

}
