package com.wex.microdsl.model

import org.yaml.snakeyaml.Yaml

/**
 * Config specifications allow a user to load any YAML files they want at generate or deploy time, and can
 *  be accessed with the `getConfig()` function.
 *
 *  @see com.wex.microdsl.deploy.model.ChildTemplateSpec#getConfig
 */
class ConfigSpec {
    Map mapping = [:]
    String name

    def name(String name) {
        this.name = name
    }

    def configFile(String filePath) {
        mapping = new Yaml().load(new File(filePath).text)
    }

    def getData(String key) {
        mapping[key]
    }
}
