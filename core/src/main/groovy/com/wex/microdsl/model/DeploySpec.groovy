package com.wex.microdsl.model

import com.wex.microdsl.Microdsl

/**
 * Base deploy spec that wraps the closures defined in the microdsl file
 */
class DeploySpec {
    static Map configMappings = [:]
    String environment = Microdsl.environment
    static String team
    static String stackName
    static String serviceName
    static String springPropertyLocation = "target/classes"

    def configs(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ConfigMappingSpec) Closure mappingClosure) {
        def configSpec = new ConfigMappingSpec()
        def c = mappingClosure.rehydrate(configSpec, this, configSpec)
        c()
        configSpec.configs.each{
            if (configMappings[it.name]) configMappings[it.name] << it.mapping
            else configMappings[it.name] = it.mapping
        }
    }

    def team(String team) {
        this.team = team
    }

    def stackName(String stackName) {
        this.stackName = stackName
    }

    def serviceName(String serviceName) {
        this.serviceName = serviceName
    }

    def springPropertyLocation(String springPropertyLocation) {
        this.springPropertyLocation = springPropertyLocation
    }
}
