package com.wex.microdsl.model

/**
 * Config mapping is a wrapper for any config specs defined
 */
class ConfigMappingSpec {
    List<ConfigSpec> configs = []

    def config(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ConfigSpec) Closure configClosure) {
        def configSpec = new ConfigSpec()
        def c = configClosure.rehydrate(configSpec, this, this)
        c()
        configs << configSpec
    }
}
