package com.wex.microdsl.model

import com.wex.microdsl.JobType
import com.wex.microdsl.Microdsl
import com.wex.microdsl.deploy.model.ChildTemplateSpec
import com.wex.microdsl.generate.model.ChildGenerateSpec

/**
 * A Child Deployment Specification. This is the wrapper for the microdsl file, and handles the actual invocation of
 *  the generate and deploy closures defined.
 */
class ChildDeploySpec extends DeploySpec {
    ChildTemplateSpec deploymentSpec
    ChildGenerateSpec generateSpec
    ParentDeploySpec parentSpec

    /**
     * When a parent is defined in the child microdsl this program will simply go out to github and grab the project.
     *  This is similar to how Jenkins handles shared libraries, and I found it pretty useful especially for testing new
     *  features, where a developer could simply point their deployment file to a feature branch of the parent
     * @param parentGit - the repo name and branch separated by a `@` symbol ex : `infrastructure@master`
     */
    def parent(String parentGit) {
        def f
        if (parentGit.contains("@")) { // indicates a remote location for the parent
            // 9.99/10 the target dir will always exist, but good to just attempt to do it every time just in case
            "mkdir target".execute().waitForProcessOutput()
            def (repo, branch) = parentGit.tokenize("@")
            // @todo mask that api key (MICRO-832)
            "git clone -b ${branch} https://microdsl:sCcJosWUWGMwSd17JJNU@gitlab.com/wexinc/${repo}.git target/parent".execute().waitForProcessOutput(System.out, System.err)
            f = new File("target/parent/deploy.microdsl").text
        } else { // assume that it is a file path if there is no '@'
            f = new File(parentGit).text
        }
            def c = new GroovyShell().evaluate("microdsl = {${f}}")
            microdsl(c)
    }

    def microdsl(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ParentDeploySpec) Closure parentClosure) {
        parentSpec = new ParentDeploySpec()
        def p = parentClosure.rehydrate(parentSpec, this, parentSpec)
        p()
    }

    def deploy(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ChildTemplateSpec) Closure temp) {
        if(Microdsl.jobType == JobType.DEPLOY) {
            deploymentSpec = new ChildTemplateSpec()
            if (parentSpec) deploymentSpec.setParent(parentSpec.deployment)
            def t = temp.rehydrate(deploymentSpec, this, this)
            t()
        }
    }

    def generate(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ChildGenerateSpec) Closure generateClosure) {
        if (Microdsl.jobType == JobType.GENERATE) {
            generateSpec = new ChildGenerateSpec()
            if (parentSpec) generateSpec.setParent(parentSpec.generate)
            def g = generateClosure.rehydrate(generateSpec, this, this)
            g()
        }
    }

    def executeService() {
        deploymentSpec.deployService()
    }
}
