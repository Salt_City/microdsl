package com.wex.microdsl.model

import com.wex.microdsl.JobType
import com.wex.microdsl.Microdsl
import com.wex.microdsl.deploy.model.ParentTemplateSpec
import com.wex.microdsl.generate.model.ParentGenerateSpec

/**
 * Parent deploy Spec
 */
class ParentDeploySpec extends DeploySpec {
    ParentTemplateSpec deployment = new ParentTemplateSpec()
    ParentGenerateSpec generate = new ParentGenerateSpec()

    def deploy(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ParentTemplateSpec) Closure temp) {
        if (Microdsl.jobType == JobType.DEPLOY) {
            def t = temp.rehydrate(this.deployment, this.deployment, this)
            t()
        }
    }

    def generate(@DelegatesTo(strategy = Closure.DELEGATE_FIRST, value = ParentGenerateSpec) Closure generateClosure) {
        if (Microdsl.jobType == JobType.GENERATE) {
            def g = generateClosure.rehydrate(generate, this, this)
            g()
        }
    }
}
