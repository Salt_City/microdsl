package com.wex.microdsl

import com.amazonaws.AmazonServiceException

class AwsUtil {

    static def safeCall(Closure c) {
        safeCallInner(c, 0)
    }

    private static def safeCallInner(Closure c, int i) {
        try {
            c()
        } catch(err) {
            if (err instanceof AmazonServiceException) {
                if (err.getErrorCode() == "Throttling" && err.getMessage().contains("Maximum sending rate exceeded")) {
                    Thread.sleep(sleepTime(i, 1000, 10000))
                    safeCallInner(c, i + 1)
                } else throw err
            } else throw err
        }
    }

    // https://en.wikipedia.org/wiki/Exponential_backoff
    static long sleepTime(int currentTry, long minSleepMillis, long maxSleepMillis) {
        def currentTime = minSleepMillis*Math.pow(2, currentTry)
        currentTime > maxSleepMillis ? maxSleepMillis : currentTime
    }
}
