package com.wex.microdsl

import com.amazonaws.services.cloudformation.AmazonCloudFormationClientBuilder
import com.amazonaws.services.cloudformation.model.DeleteStackRequest
import com.amazonaws.services.cloudformation.model.DescribeStacksRequest
import com.amazonaws.services.cloudformation.model.ValidateTemplateRequest
import com.amazonaws.waiters.WaiterParameters
import org.apache.maven.model.io.xpp3.MavenXpp3Reader
import org.apache.maven.project.MavenProject
import org.junit.Test

class MicrodslTest {
    @Test
    void testAllGenerateDeployments() {
        new File(getClass().getResource("/microdsl").toURI()).listFiles().each {
            generateValidateDeploy(it.path)
        }
    }

    @Test
    void singleTest() {
        generateValidateDeploy(getClass().getResource("/microdsl/basicParent2.microdsl").toURI().path)
    }

    private def generateValidateDeploy(String microdsl) {
        // Generate AWS template
        generate(microdsl)

        // Validate template
        assert validate()

        // Deploy to dit
        assert deploy(microdsl)

        // Remove from dit
        remove(microdsl.find("\\d+"))
    }

    private def generate(String deploy) {
        Microdsl.generate(deploy, getClass().getResource("/").toURI().path, new MavenProject())
    }

    private def validate() {
        def file = new File(getClass().getResource("/service.yml").toURI().path).text
        try {
            AmazonCloudFormationClientBuilder.standard().withRegion("us-east-1").build()
                    .validateTemplate(new ValidateTemplateRequest().withTemplateBody(file))
            true
        } catch (e) {
            println e.getMessage()
            false
        }
    }

    private def deploy(String deploy) {
        MavenProject project = new MavenProject(new MavenXpp3Reader().read(new FileReader(getClass().getResource("/pom.xml").toURI().path)))
        Microdsl.deploy(deploy, new MavenProject(), "dit" )
    }

    private def remove(number) {
        def client = AmazonCloudFormationClientBuilder.standard().withRegion("us-east-1").build()
        println "[INFO] Deleting stack(s)..."
        client.describeStacks().getStacks()
                .findAll{it.getStackName().contains("test-dit") && it.getStackName().contains(number)}
                .each{
                    println "[INFO] Deleting ${it.getStackName()}..."
                    client.deleteStack(new DeleteStackRequest().withStackName(it.getStackName()))
                    client.waiters().stackDeleteComplete().run(new WaiterParameters<>(new DescribeStacksRequest().withStackName(it.getStackName())))
                    println "[INFO] Deleted ${it.getStackName()}"
                }
    }
}
